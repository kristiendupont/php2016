<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden removePersonFromArray skal kunne fjerne en person fra et indexeret array.
     * Brug den indbyggede metode array_slice().
     * Se kapitel - Array -> Extracting multiple values og afsnittet Slicing Array
     */
    
    class Person
    {
        function removePersonFromArray()
        {
            $people = array("Tom", "Dick", "Harriet", "Brenda", "Jo");
            $middle = array_slice($people,1,3); //her oprettes der et nyt array, der tager værdierne fra det angivne array. x,x angiver hvor i det orginale array der skal skæres og hvor mange steps hen der skal slices
            echo $people[0] . " " . $people[1] . " " . $people[2] . " " . $middle[0];
        }
    }
    $person = new Person;
    $person->removePersonFromArray();
?>