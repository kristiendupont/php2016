<?php
    /*
    * Sammenligningsoperatorer
    * ==, ===, !=, !==, >, >=, <, <=
    * == spørger om værdien er ens
    * === evaluerer på om værdien og datatypen er ens
    * != evaluerer på om værdien er forskellig
    * !== evaluerer på om værdien og datatypen er forskellig
    * Sammenligningsoperatorer returnerer sandt eller falsk
    */

    //To variabler med forskellig værdier
    $a = 1;
    $b = 2;
    echo '$a == $b : ';
    if($a==$b)
    {
        echo "Denne sammenligningsoperator returnerer sandt i denne eksempel.";
    }else {
        echo "Denne sammenligsningsoperator returnerer falsk i dette eksempel";
    }
    echo "<br>";

    //To variabler med samme værdi men forskellige datatyper. Værdierne testes.
    $a = "2";
    $b = 2;
    echo '$a == $b : ';
    if($a==$b)
    {
        echo "Denne sammenligningsoperator returnerer sandt i denne eksempel.";
    }else {
        echo "Denne sammenligsningsoperator returnerer falsk i dette eksempel";
    }
    echo "<br>";

    //To variabler med samme værdi men forskellige datatyper. Det testes om værdier og type identisk. 
	$a = "2";
    $b = 2;
    echo '$a === $b : ';
    if($a===$b)
    {
        echo "Denne sammenligningsoperator returnerer sandt i denne eksempel.";
    }else {
        echo "Denne sammenligsningsoperator returnerer falsk i dette eksempel";
    }
    echo "<br>";

    //To variabler med forskellig værdier. Det testes om værdierne er forskellige fra hinanden.
    $a = 1;
    $b = 2;
    echo '$a != $b : ';
    if($a!=$b)
    {
        echo "Denne sammenligningsoperator returnerer sandt i denne eksempel.";
    }else {
        echo "Denne sammenligsningsoperator returnerer falsk i dette eksempel";
    }
    echo "<br>";

    //To variabler med ens værdier men forskellig datatyper. 
    $a = "2";
    $b = 2;
    echo '$a !== $b : ';
    if($a!==$b)
    {
        echo "Denne sammenligningsoperator returnerer sandt i denne eksempel.";
    }else {
        echo "Denne sammenligsningsoperator returnerer falsk i dette eksempel";
    }
    echo "<br>";

?>