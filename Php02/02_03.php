<?php
    /*
    * Autoincrement og decrement
    * ++ og --
    */

    $animals = array('And','Gås','Svane','Spurv','Pade','Ål');
    $counter = 0;
    //Hvor hvor mange dyr er set i søen

    foreach ($animals as $animal) {
        $counter++; //++ kan sættes både i forenden og bagenden - Klaus vil gerne have at de to tegn skal være lagt på efter
    }
    echo $counter;
    echo "<br>";

    //Decrement
    $animals = array('And','Gås','Svane','Spurv','Pade','Ål');
    $counter = 6;
    //Hvor hvor mange dyr er set i søen

    foreach ($animals as $animal) {
        $counter--; //-- kan sættes både i forenden og bagenden - Klaus vil gerne have at de to tegn skal være lagt på efter
    }
    echo $counter; 
    echo "<br>";
    

?>