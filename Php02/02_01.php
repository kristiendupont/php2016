<?php

    /*
     * Operatorer
     *
     */

    // Addition - kaldes tildelingsopreator
    $a = 1+2;
    echo $a;
    echo "<br>";

    //To variablers værdier adderet
    $b = 2;
    $c = 2;
    echo $b+$c;
    echo "<br>";

    //Subtraktion
    $a = 1-2;
    echo $a;
    echo "<br>";

    //To variablers værdier subtraheret
    $b = 4;
    $c = 4;
    echo $b-$c;
    echo "<br>";

    //Multiplication
    $a = 2*2;
    echo $a;
    echo "<br>";
    
    //To variablers værdier multipliceret
    $b = 2;
    $c = 2;
    echo $b*$c;
    echo "<br>";
?>