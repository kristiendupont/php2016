<?php
    /* Dette dokument skal indeholde de dataelementer der indgår i html siden.
     * Følgende dataelementer skal som minimum være udpenslet i dette dokument.:
     * - email 
     * - phone
     * - preferences
     * - jobs
     * - competencies
     *
     * Det vil være naturligt at lade ovenstående elementer være 
     */

     $first_name = "Kristine ";
     $middle_name = "Dupont ";
     $last_name = "Johannessen";
     $birthday = 131192;

     $email = "kris819h@student.eadania.dk";
     $phone = 31120429;

     $preferences = array('Programmering','Organisation og udvikling','Design og kommunikation');

     $studyplaces = array('VIAUC Skive' => 'Folkeskolelærer','VIAUC Viborg' => 'Pædagog','Dania' => 'Multimediedesigner');
     
     $competencies = array('Interaktion',array('HTML','CSS','JavaScript - spirende','PHP - spirende'),array('Brackets','Dreamweaver','Visual Studio Code'));

?>