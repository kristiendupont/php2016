<?php
    /*Datatyper, string eller tekststrengværdier*/

    $a = "Dette er en tekststreng<br>";
    echo $a;

    $a = 'Dette er en tekststreng<br>';
    echo $a;

    $name = "Kristine";
    echo "Hej, $name";
    echo "<br>";
    echo 'Hej, ' . $name; //Skrives $name ind i 'Hej, $name', vil strengen ikke kunne se variablen - den vil blive betragtet som en del af strengen
    
    $a = "<p>Dette er en tekststreng der indeholder afsnitselementer </p>";
    echo $a;

    date_default_timezone_set("Europe/Copenhagen");
    $a = "<div>Dato i dag er: " . date("d.m.Y") . " og kl. er: " . date("h:i");
    echo $a;

    //Teste om to strenge er extension_loaded
    echo "<br>";
    echo "Er de to variabler ens?<br>";
    $a = "abc";
    $b = "abd";

    if($a == $b){
        echo 'Ja $a og $b er ens <br>';
    } else {
        echo 'Nej, $a og $b er forskellige.';
    }
?>