<?php
    //Datatyper og variabler. Dette dokument indlejres i det efterfølgende.

    //tekststreng (string)
    $firstName = "Kristine";

    //tekststreng (string)
    $lastName = " Dupont Johannessen";

    //numerisk heltal (integer)
    $age = 23;

    //boolean (sandt/falsk)
    $inRelationship = true;

    //tekststreng
    $work = "Studerende og handicaphjælper";

    //tekststreng
    $workPlace = "EA Dania og Privat";

    //array (en række)
    $hobbies = ["Spil", "Harry Potter", "andre kreative aktiviteter"];
?>