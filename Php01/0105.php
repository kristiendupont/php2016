<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Indsæt variabler</title>
    </head>
 
    <body>

        <p>Nedenunder vil jeg forsøge at indsætte mine variabler i dokumentet.</p>

        <?php 
            include("0104.php");

            echo "<p>Fornavn: $firstName</p>";
            echo "<p>Efternavn: $lastName</p>";
            echo "<p>Alder: $age</p>";
            if($inRelationship == true){
                echo "<p>Status: I et forhold</p>";
            } else {
                echo "<p>Status: Single</p>";
            }
            echo "<p>Beskæftigelse: $work ved $workPlace</p>";
            echo "<p>Bruger fritiden på: $hobbies[0], $hobbies[1] og $hobbies[2]</p>";
        ?>

    </body>
</html>

